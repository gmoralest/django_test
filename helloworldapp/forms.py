from django import forms
from helloworldapp.models import Person

class FormPerson(forms.ModelForm):

    class Meta:
        model = Person
        fields = ('name',)