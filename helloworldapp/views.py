from django.shortcuts import render,redirect
from helloworldapp.models import Person
from helloworldapp.forms import FormPerson

# Create your views here.

from django.http import HttpResponse
# def foo(request):
#      name = "Bogo"
#      html = "<html><body>Hello World! from %s</body></html>" % name
#      return HttpResponse(html)

def foo(request,):
    return render(request, "helloDJ/base.html",
       {"Testing" : "Django Template Inheritance ",
       "HelloHello" : "Hello World - Django"})

def fooname(request, name): # helloworld/bogo
     html = "<html><body>Hello World! from %s</body></html>" % name
     return HttpResponse(html)

def helloworld2(request, name):  #hierarchical template
    return render(request, "helloDJ/helloworlrd3.html", {"name" : name})

def helloworld5(request):
    return render(request,"helloDJ/helloworld5.html", {"person_list" : Person.objects.all()} )
    #return render(request,"helloDJ/helloworld4.html", {"person_list" : "caca"} )

def helloworld6(request,): # read css
    return render(request, "helloDJ/helloworld6.html", {"name" : "Bogo"})

def helloworld7(request,): 
    return render(request, "helloDJ/helloworld7.html")

def helloworld8(request,): # internal link to helloworld7
    return render(request, "helloDJ/helloworld8.html")

def tablePage(request,): #
    return render(request, "helloDJ/tablePage.js")

def formsearch(request):

    if request.method == "POST":
        form = FormPerson(request.POST)
        if form.is_valid():
            formperson = form.save(commit=False)
            return redirect('/formresult/{}/'.format(formperson.name))
    else:
        form = FormPerson()
    return render(request, 'helloDJ/formsearch.html', {'form': form})

def formresult(request, name):
        html = """<html><h1>Search result</h1><body>
        This is the name that was searched: {}</body></html>""".format(name)
        return HttpResponse(html)