"""helloworld URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from helloworldapp.views import foo,fooname,helloworld2, helloworld5, helloworld6, helloworld7, helloworld8, tablePage,formsearch, formresult
from django.contrib import admin

urlpatterns = [
    path('home/', admin.site.urls) ,
    path('helloworld/', foo),
    path('helloworld/<str:name>/', fooname),
    path('helloworld2/<name>/', helloworld2),
    #seance2
    path('helloworld5/', helloworld5), #read database
    path('helloworld6/', helloworld6),
    path('helloworld7/', helloworld7, name='helloworld7'),
    path('helloworld8/', helloworld8, name='helloworld8'),
    path('tablePage/', tablePage),
    path('formsearch/', formsearch),
    path('formresult/<str:name>/', formresult),
    
]
